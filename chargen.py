import random
import sys

from utils import names


class Race:
    def __init__(self, name, min_age, max_age):
        self.name = name
        self.min_age = min_age
        self.max_age = max_age


def get_gender():
    return random.choice(['male', 'female', 'other'])


def get_country():
    return random.choice(['Sagelight', 'Alliance, Eland', 'Alliance, Madiy', 'Xim\'Ripa', 'Dalgaz', 'Buckrock', 'Haverton', 'Dominia', 'Saren', 'Fidrune', 'Mad Sorceror', 'Conclave of Innovation', 'Onra Thran', 'Chaol', 'Regoro Goyar'])


def get_race_by_country(country):
    goblin = Race('goblin', 15, 30)
    orc = Race('orc', 20, 150)
    kobold = Race('kobold', 12, 25)
    sea_elf = Race('sea elf', 18, 1000)
    iron_elf = Race('iron elf', 18, 1000)
    dark_elf = Race('dark elf', 18, 1000)
    wood_elf = Race('wood elf', 18, 1000)
    dwarf = Race('dwarf', 12, 300)
    half_elf = Race('half elf', 18, 250)
    human = Race('human', 18, 120)
    halfling = Race('halfling', 16, 100)
    faun = Race('faun', 16, 80)
    chirri = Race('chirri', 14, 50)

    all_races = [goblin, orc, kobold, sea_elf, iron_elf, dark_elf, wood_elf, dwarf, half_elf, human, halfling, faun]
    country_race_map = {
        'sagelight': all_races,
        'alliance, eland': [iron_elf, dark_elf, wood_elf, dwarf, half_elf, human, halfling, faun],
        'alliance, madiy': [iron_elf, dark_elf, wood_elf, dwarf, half_elf, human, halfling, faun],
        'xim\'ripa': [(sea_elf, 2), (iron_elf, 90), (dark_elf, 2), (half_elf, 6)],
        'dalgaz': [goblin, orc, kobold, faun],
        'buckrock': [(dwarf, 90), (human, 8), (halfling, 2)],
        'haverton': [sea_elf, iron_elf, dark_elf, wood_elf, dwarf, half_elf, human, halfling, faun],
        'dominia': [dwarf, half_elf, human, halfling],
        'saren': [sea_elf, dark_elf, wood_elf, dwarf, half_elf, human, halfling, faun],
        'fidrune': [wood_elf, dwarf, half_elf, human],
        'mad sorceror': all_races,
        'conclave of innovation': all_races,
        'onra thran': [(wood_elf, 10), (dwarf, 80), (human, 10)],
        'chaol': [chirri],
        'regoro goyar': [dwarf, human, faun, sea_elf],
    }

    country_map = country_race_map[country.lower()]

    if type(country_map[0]) is tuple:
        r = random.randint(1, 100)
        for i in range(len(country_map)):
            if r < country_map[i][1]:
                return country_map[i][0]
            r = r - country_map[i][1]
        return country_map[0]
    else:
        return random.choice(country_map)


def get_affinity():
    affinities = ['major fire', 'minor fire', 'major water', 'minor water', 'major air', 'minor air', 'major earth', 'minor earth', 'no affinity', 'void']
    aff = random.choice(affinities)
    if aff.startswith('major'):
        aff = aff + ', ' + random.choice(affinities)
    if 'void' in aff:
        aff = 'void affinity'
    return aff


def get_alignment():
    return random.choice(['lawful good', 'neutral good', 'chaotic good', 'lawful neutral', 'true neutral', 'chaotic neutral', 'lawful evil', 'neutral evil', 'chaotic evil'])


def get_class():
    return random.choice(['assassin', 'healer', 'scholar', 'mage', 'warrior', 'thief', 'battlemage', 'shadowblade', 'noble', 'worker', 'shopkeeper', 'innkeeper', 'politician'])


def get_name(gender):
    return '%s %s%s' % (names.get_first_name(gender.title()), names.get_first_name() + ' ' if random.randint(1, 100) < 5 else '', names.get_last_name())


def get_realm(country):
    realm = 'lurele goyar'
    if country == 'fidrune' or country == 'chaol' or country == 'onra thran' or country == 'regoro goyar':
        realm = country
    return realm


def main(argv):

    gender = get_gender()
    country = get_country()
    race = get_race_by_country(country)
    affinity = get_affinity()
    alignment = get_alignment()
    c = get_class()

    current_year = 1020

    name = get_name(gender)

    realm = get_realm(country)
    age = random.randint(race.min_age, race.max_age)
    birth = current_year - age

    print("%s (%s)\n%s, %s %s, %s\n%s\n%iCE - now (%i years old)\n%s" % (name.upper(), affinity, realm.title(), race.name.title(), gender.title(), alignment.title(), c.title(), birth, age, country))


if __name__ == '__main__':
    main(sys.argv[1:])
