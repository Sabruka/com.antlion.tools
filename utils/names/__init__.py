from __future__ import unicode_literals
from os.path import abspath, join, dirname
import random


"""
FORK OF names

USED UNDER MIT LICENSE.
"""


__title__ = 'names'
__version__ = '0.3.0'
__author__ = 'Trey Hunner'
__license__ = 'MIT'


full_path = lambda filename: abspath(join(dirname(__file__), filename))


FILES = {
    'first:male': full_path('dist.male.first'),
    'first:female': full_path('dist.female.first'),
    'last': full_path('dist.all.last'),
}

male_first = []
female_first = []
last = []

with open(full_path('dist.male.first')) as f:
    lines = f.read().splitlines()
    for line in lines:
        parts = line.split()
        male_first.append(parts[0])

with open(full_path('dist.female.first')) as f:
    lines = f.read().splitlines()
    for line in lines:
        parts = line.split()
        female_first.append(parts[0])

with open(full_path('dist.all.last')) as f:
    lines = f.read().splitlines()
    for line in lines:
        parts = line.split()
        last.append(parts[0])


def get_name(filename):
    selected = random.random() * 90
    with open(filename) as name_file:
        for line in name_file:
            name, _, cummulative, _ = line.split()
            if float(cummulative) > selected:
                return name
    return ""  # Return empty string if file is empty


def get_first_name(gender=None, r=random):
    if gender not in ('male', 'female'):
        gender = r.choice(('male', 'female'))
    if gender == 'male':
        return r.choice(male_first)
    else:
        return r.choice(female_first)


def get_last_name(r=random):
    return r.choice(last)


def get_full_name(gender=None, r=random):
    return "{0} {1}".format(get_first_name(gender, r), get_last_name(r))
